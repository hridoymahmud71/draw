<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

//my-good-home_1.xml is original
//my-good-home_2.xml is the vertical interchange
//my-good-home_2.xml is the horizontal interchange

if (!isset($_GET['url'])) {
    header('Content-type: text/xml');
    echo file_get_contents('my-good-home_1.xml');
} else {
    if ($_GET['url'] == '1' || $_GET['url'] == '') {
        echo file_get_contents('my-good-home_1.xml');
    } else if ($_GET['url'] == '2') {
        echo file_get_contents('my-good-home_2.xml');
    } else if ($_GET['url'] == '3') {
        echo file_get_contents('my-good-home_3.xml');
    }else if ($_GET['url'] == '4') {
        echo file_get_contents('my-good-home_4.xml');
    }else if ($_GET['url'] == '5') {
        echo file_get_contents('my-good-home_5.xml');
    }else if ($_GET['url'] == '6') {
        echo file_get_contents('my-good-home_6.xml');
    }else if ($_GET['url'] == '7') {
        echo file_get_contents('my-good-home_7.xml');
    }else if ($_GET['url'] == '8') {
        echo file_get_contents('my-good-home_8.xml');
    }else {
        echo file_get_contents('my-good-home_1.xml');
    }
}

