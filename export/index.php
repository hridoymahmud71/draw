<?php

include_once("../php/src/mxServer.php");

error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', 1);



// Gets the format parameter from the URL
$format = $_REQUEST["format"];
// Gets the XML parameter from the POST request
$html = $_REQUEST["html"];


if (isset($html) && isset($format)) {

    $str = date('Ymdhis') . rand(1000, 9999) . time();
    $filename = $str . '.' . $format;

    $myfile = fopen($filename, "w") or die("Unable to open file!");
    fwrite($myfile, $html);
    fclose($myfile);

    if (isset($_REQUEST["todo"])) {
        if ($_REQUEST["todo"] == "download") {

            header("Content-type:application/octet-stream");
            // It will be called downloaded.pdf
            header("Content-Disposition:attachment;filename={$filename}");
            // The PDF source is in original.pdf
            readfile($filename);
        } else if ($_REQUEST["todo"] == "email") {
            //--------------------------------------------------------------------
            $mailto = isset($_REQUEST["email"]) ? $_REQUEST["email"] : 'mahmud@sahajjo.com@gmail.com';
            $from_name = 'RS Draw';
            $from_mail = 'rsdraw@royalbanq.com';
            $replyto = $from_mail;
            $subject = "Norgeshus house plan";
            $message = isset($_REQUEST["message"]) ? $_REQUEST["message"] : "You have a new home image";
            //---------------------------------------------------------------------

            $content = file_get_contents(__DIR__ . '/' . $filename);
            $content = chunk_split(base64_encode($content));
            $uid = md5(uniqid(time()));
            $name = basename($filename);

            // header
            $header = "From: " . $from_name . " <" . $from_mail . ">\r\n";
            $header .= "Reply-To: " . $replyto . "\r\n";
            $header .= "MIME-Version: 1.0\r\n";
            $header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\r\n\r\n";

            // message & attachment
            $nmessage = "--" . $uid . "\r\n";
            $nmessage .= "Content-type:text/plain; charset=iso-8859-1\r\n";
            $nmessage .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
            $nmessage .= $message . "\r\n\r\n";
            $nmessage .= "--" . $uid . "\r\n";
            $nmessage .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"\r\n";
            $nmessage .= "Content-Transfer-Encoding: base64\r\n";
            $nmessage .= "Content-Disposition: attachment; filename=\"" . $filename . "\"\r\n\r\n";
            $nmessage .= $content . "\r\n\r\n";
            $nmessage .= "--" . $uid . "--";

            if (@mail($mailto, $subject, $nmessage, $header)) {
                echo 'Mail Sent';
            } else {
                echo 'Mail failed';
            }
        }
    }

    unlink(__DIR__ . '/' . $filename);
    die();
}
?>
